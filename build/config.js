import path from 'path'

const config = new Map();

config.set('host','localhost')
config.set('port', 3000)

config.set('host_port',
  `http://${config.get('host')}:${config.get('port')}`
);

config.set('proxy',{
	"/api": {
    target: "http://localhost:8000", 
    pathRewrite: {"^/api" : ""},
    secure: false              
  }
})

//     { from: /^\/$/, to: 'index.html' },
config.set('vendor_dependencies', [
  'vue'
]);

config.set('build_dependencies',[
  'babel-polyfill',
  'classlist-polyfill'  
])

config.set('product_dependencies', config.get('vendor_dependencies').concat(config.get('build_dependencies')))

config.set('bootstrap_ui', [
	"admin-lte/bootstrap/css/bootstrap.css"
]);


config.set('vendor_ui', [
	"admin-lte/less/AdminLTE.less",
	"admin-lte/less/skins/skin-blue.less"
]);

config.set('assetsRoot',"dist");
config.set('assetsPublic','/assets/');
config.set('assetsRootPath',path.join(config.get('assetsRoot'),config.get('assetsPublic')))

config.set('isBuild',process.env.NODE_ENV === 'production')

export default config
