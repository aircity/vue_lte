import webpack from 'webpack'
import webpackConfig from './webpack.js'
import config from './config.js';
import webpackServer from 'webpack-dev-server'
import ProgressPlugin from 'webpack/lib/ProgressPlugin'

import open from './lib/open'

import merge from 'webpack-merge';

const App = {};
App.config = webpackConfig;

App.startup = (baseWebpackConfig) => {
    let webpackConfig = baseWebpackConfig ? baseWebpackConfig : App.config;
    webpackConfig.entry.app.unshift(
        `webpack-dev-server/client?${config.get("host_port")}`,
        'webpack/hot/dev-server'
    )
    webpackConfig.performance = { hints: false };    
    webpackConfig.plugins.push(
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()  
    )
    let compiler = webpack(webpackConfig);
    compiler.apply(new ProgressPlugin())
    const Server = new webpackServer(compiler, {
        inline: true,
        contentBase: "/",      
        hot: true,
        historyApiFallback:true,
        stats: {
            colors: true,
            chunks: false
        },    
        proxy: config.get("proxy")    
    })

    let isStart = false;
  	compiler.plugin("done", function (stats) {
  		if (stats.hasErrors()) {
  			console.log("compiler errors")
  			return;
  		}
  		if (isStart) {
  			return;
  		}
  		Server.listen(config.get('port'), '0.0.0.0', function () {
  			console.log('Server running at:', config.get("host_port"));
  			isStart = true
  		})
  	});
}

App.compile = () => {
    let baseWebpackConfig = App.config;
    const webpackConfig = merge(baseWebpackConfig, {
        performance: { hints: false },   
        devtool: "cheap-module-source-map",
        output: {
            path: config.get('assetsRootPath'),
            filename: 'script/[name].[chunkhash].js',
            chunkFilename: 'script/[name].chunk.[chunkhash].js',
            publicPath: config.get("assetsPublic")
        },
        plugins: [
            new webpack.LoaderOptionsPlugin({
                minimize: true
            }),
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: true,
                compress: {
                    warnings: false
                },
                output: {
                    comments: false
                }
            }),
            new webpack.DefinePlugin({
              'process.env': {
                NODE_ENV: '"production"'
              }
            })                            
        ]
    });

    let compiler = webpack(webpackConfig)
    compiler.apply(new ProgressPlugin())
    compiler.run(function(err, stats) {
        if (err) throw err
        process.stdout.write(stats.toString({
            colors: true,
            modules: false,
            children: false,
            chunks: false,
            chunkModules: false
        }) + '\n')
    })
}

export default App;
