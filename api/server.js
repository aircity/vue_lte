'use strict';

const Hapi = require('hapi');

const _ = require('lodash');

const faker = require('faker');

const server = new Hapi.Server();
server.connection({ 
    host: 'localhost', 
    port: 8000 
});

server.route({
    method: 'GET',
    path:'/get', 
    handler: function (request, reply) {
        return reply('get it');
    }
});

server.route({
    method: 'POST',
    path: '/dataset',
    handler: function (request, reply) {
			let aoData = request.payload.aoData;
			let start = JSON.parse(aoData)[0].value;
			let length = JSON.parse(aoData)[1].value;
					start = (start-1) * length +1;
			let data = {
				total: 100,
				user: _.times(length,function(index) {					
					return {
						_id: start+index,
						name:faker.name.findName(),
						position: faker.name.jobTitle(),
						state: faker.random.boolean()?1:0,
						phone: faker.phone.phoneNumberFormat(),
						date: faker.date.past()						
					}						
				}),
        respCode: "00"
			}

      return reply(data);
    }
})

server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});