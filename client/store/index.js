import Vuex from 'vuex'
import menu from 'router/menu'

Vue.use(Vuex)

const state = {
  menu
}

const mutations = {
  SIDEBAR (state, status = true) {
    state.config.sidebar = status
  }
}

const actions = {
  SHOW_SIDEBAR ({ commit }) {
    commit('SIDEBAR', true)
  },
  HIDE_SIDEBAR ({ commit }) {
    commit('SIDEBAR', false)
  },
  TOGGLE_SIDEBAR ({ commit, state }) {
    commit('SIDEBAR', !state.config.sidebar)
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
