import Router from 'vue-router'
import Layout from 'layout/layout.vue'
import Login from 'layout/login.vue'
import Home from 'layout/home.vue'
import menu from './menu';

Vue.use(Router);

menu.unshift({name: 'Home', path: '/', component: Home , appOnly: true})

const router = new Router({
    mode: 'history',
    base: '/',  
    linkActiveClass: 'active',
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '/',
            component: Layout,
            children: menu
        }, {
            name: 'Login',
            path: '/login',
            component: Login
        }, {
            path: '*',
            redirect: '/'
        }
    ]
})

router.beforeEach((route, redirect, next) => {
    next()
})

router.afterEach((route, redirect) => {})

export default router
