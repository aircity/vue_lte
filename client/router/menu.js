import Layout from 'layout/layout.vue';
import Mounter from './mounter.vue';

const lazy = (name) => () => System.import(`pages/${name}.vue`);

export default [
  {
    title: '表单演示',
    path: '#form',
    component: Mounter,
    meta: {
      icon: 'fa-credit-card'
    },    
    children: [
      {        
        title: '提交表单',
        path: '/form/form',
        component: lazy('form')      
      },
      {        
        title: '时间控件',
        path: '/form/dates',
        component: lazy('dates'),
        meta: {
          icon: 'fa-credit-card'
        }        
      }      
    ]
  },
  {
    title: '组件演示',
    path: '#component',
    component: Mounter,
    meta: {
      icon: 'fa-windows'
    },        
    children: [
      {
        title: '模态框',        
        path: '/component/modal',
        component: lazy('modal')
      }      
    ]
  } 
]
