import 'lib/http.js';

//注册全局组件
import 'component';

//注册全局事件

const Midway = {};

window.Midway = Midway;

import Alert from 'component/modals/alert.vue'
const AlertComponent = Vue.extend(Alert)

Midway.alert = function (data) {
  let propsData = {};
  if(typeof data == "string") {
    propsData = {
      content: data
    }
  } else {
    propsData = data || {}
  }  
  new AlertComponent({
    el: document.createElement('div'),
    propsData
  })    
}

import Confirm from 'component/modals/confirm.vue'
const ConfirmComponent = Vue.extend(Confirm)

Midway.confirm = function (data) {
  let propsData = {};
  if(typeof data == "string") {
    propsData = {
      content: data
    }
  } else {
    propsData = data || {}
  }  
  
  let Component = new ConfirmComponent({
    el: document.createElement('div'),
    propsData
  })    
  
  return new Promise(function(resolve,reject){
			Component.$on("resolve",function(){
				this.$destroy();
				resolve();
			}).$on("reject",function(){
			  this.$destroy();
			})
		})
}