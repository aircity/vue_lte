import axios from 'axios'

axios.interceptors.request.use(function (request) {
  return request;
}, function (error) {
  return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
  return response.data;
}, function (error) {
  return Promise.reject(error);
});

Vue.prototype.$http = axios