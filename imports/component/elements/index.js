import Button from './button.vue'
Vue.component("Button",Button);

import Form from './form.vue';
Vue.component("Form",Form);

import Box from './box.vue';
Vue.component("Box",Box);

import Date from './date.vue';
Vue.component("Date",Date);